import React from 'react';
import ReactDOM from 'react-dom';
import Main from './components/main.jsx'

document.addEventListener("DOMContentLoaded", function(event) { //when the DOM is ready IE 9 will make problems
  var application = document.getElementById('hiragana');
  const hiragana = ReactDOM.render(<Main />, application);
});
