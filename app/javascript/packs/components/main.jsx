import React from 'react';
import ReactDOM from 'react-dom';
import {DrawMask} from './canvas_elements/mask';
import {DrawBorders} from './canvas_elements/borders';
import {styles} from './constants';

class Main extends React.Component {
  constructor(props){
    super(props);
    this.handleShowClick = this.handleShowClick.bind(this);
    this.handleNewClick = this.handleNewClick.bind(this);
  };

  handleShowClick() {
    console.log("show");
  };

  handleNewClick() {
    console.log("new");
  };

  componentDidMount() {
    this.updateCanvas();
  };

  updateCanvas() {
    const canvas = this.refs.canvas;
    const canvasHeight = canvas.height;
    const canvasWidth = canvas.width;
    const ctx = canvas.getContext('2d');
    // drawing surface
    ctx.fillStyle="#FFFFFF";
    ctx.fillRect(0,0, 400, 400);

    // borders - component that makes borders and shows buttns
    DrawBorders({ctx, h: canvasHeight, w: canvasWidth, color: "#000000"});

    // mask - component that writes the task and hides the solution
    DrawMask({ctx, x: 400, y: 0, w: 400, h: canvasHeight, color: "#000000"});
  };

  render(){
    return (
      <div>
        <div style={styles.header}>
          <h1 style={styles.title}> Hiragana </h1>
          <div style={styles.buttonContainer}>
            <button style={styles.button} onClick={this.handleShowClick}> SHOW THE SOLUTION </button>
            <button style={styles.button} onClick={this.handleNewClick}> NEW CHARACTER </button>
          </div>
        </div>
        <div>
          <canvas ref="canvas" width={800} height={550} />
        </div>
      </div>
    ); 
  };

};
export default Main;
