export function DrawMask(props) {
  //door src https://s-media-cache-ak0.pinimg.com/originals/7a/db/15/7adb15f8efdfd6de606e0294a5115412.jpg
  const {ctx, x, y, w, h, color} = props;

  //background color
  ctx.fillStyle=color;

  //load the image
  const maskImg = new Image();
  maskImg.src = require('../../../../assets/images/door.jpg');

  maskImg.onload = () => {
    //background 
    ctx.fillRect(x, y, w, h);

    // change the image width 
    var canImgRatio = h/maskImg.height;
    var imgWidth = maskImg.width * canImgRatio

    //image
    ctx.drawImage(maskImg, x, y, imgWidth, h );
  };
}
