export function DrawBorders(props) {
  const {ctx, h, w, color} = props;

  //background color
  ctx.fillStyle=color;

  //load the image
  const borderImg = new Image();
  borderImg.src = require('../../../../assets/images/wood.jpg');

  var topBottomHeight = h/20;

  borderImg.onload = () => {

    //image top
    ctx.drawImage(borderImg, 0, 0, w, topBottomHeight);
    //image bottom
    ctx.drawImage(borderImg, 0, h-topBottomHeight, w, topBottomHeight);

    //side
    ctx.fillRect(0, 0, 20, h);
  };
}
