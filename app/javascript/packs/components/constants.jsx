export const styles = {
  header: {
    width: "800px"
  },
  title: {
    margin: "0",
    display: "inline-block"
  },
  buttonContainer: {
    float: "right"
  },
  button: {
    backgroundColor: "#FFFFFF",
    border: "2px solid #555555",
    color: "black",
    padding: "16px 32px",
    textAlign: "center",
    textDecoration: "none",
    display: "inline-block",
    fontSize: "16px",
    margin: "4px 2px",
    cursor: "pointer",
    outline: "none",
    boxShadow: "none"
  }
}
